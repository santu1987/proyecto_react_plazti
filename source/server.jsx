import http from 'http';
import React from 'react';
import { renderToString, renderToStaticMarkup  } from 'react-dom/server';
import {ServerRouter, createServerRenderContext } from 'react-router';
//import { StaticRouter } from 'react-router-dom';
import {IntlProvider} from 'react-intl';

import Pages from './pages/container/Page.jsx';
import Layout from './pages/components/Layout.jsx';

import messages from './messages.json';
//Funcion para crear un h1 que diga hola
function requestHandler(request,response){

	const locale = request.headers['accept-language'].indexOf('es')>=0 ? 'es' : 'en';

	const context = createServerRenderContext();
	//const context = {};

	let html = renderToString(
		<IntlProvider locale={locale} messages={messages[locale]}>
			<ServerRouter location={request.url} context={context}>
				<Pages/>
			</ServerRouter>
		</IntlProvider>,	
	);
	/*let html = renderToString(
			<StaticRouter location={request.url} context={context}>
				<Pages />
			</StaticRouter>
	);*/
	//--Para saber que esta pasando...
	const result = context.getResult();
	//--Para que el navegador mueste el html y no un string
	response.setHeader('Content-Type','text/html');

	if(result.redirect){//Hubo un redirect y hay que ejecutarlo
		response.writeHead(301,{
			Location: result.redirect.pathname,
			//Location : context.url,
		});
		response.end();//Sin enviar nada
	}
	if(result.redirect){//Hubo un redirect y hay que ejecutarlo
		response.writeHead(404);
		html = renderToString(
			<IntlProvider locale={locale} messages={messages[locale]}>
				<ServerRouter location={request.url} context={context}>
					<Pages/>
				</ServerRouter>
			</IntlProvider>,	
		);
	}
	response.write(
		renderToStaticMarkup(<Layout title="Aplicación" content={html}/>)
	);//escribo el html
	response.end();
}


//Configuro mi server
const server = http.createServer(requestHandler);
server.listen(3000);