import fetch from 'isomorphic-fetch';
//url de jsonplaecholde rde donde obtendrmeos nuestra api
const baseUrl = 'https://jsonplaceholder.typicode.com';
//Creamnos el api
const api = {
	posts:{
		//Metodo 1
		async getList(page=1){
			//nos permite hacer llamados asincronos con promesas
			//Cuando fetch temrine nos va a devolver un objeto con toda la respuesta
			const response = await fetch(`${baseUrl}/posts?_page=${page}`);
			//obtenemos la data en json
			const data = await response.json();
			return data;
		},
		//Metodo 2: devuleve un solo registro
		async getSingle(id=1){
			const response = await fetch(`${baseUrl}/posts/${id}`);
			const data= await response.json();
			return data;
		},
		//Metodo 3: Recibe un id y retorna los comentarios de un post
		async getComments(id=1){
			const response = await fetch(`${baseUrl}/posts/${id}/comments`);
			const data= await response.json();
			return data;
		},

	},
	users:{
		async getSingle(id=1){
			const response = await fetch(`${baseUrl}/users/${id}`);
			const data= await response.json();
			return data;
		},
		async getPosts(id=1){
			const response = await fetch(`${baseUrl}/posts/?userId=${id}`);
			const data= await response.json();
			return data;
		}
	},
};


export default api;