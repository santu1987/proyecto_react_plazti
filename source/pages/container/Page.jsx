import React from  'react';
import{
	Match,//Que hacer en cada ruta
	Miss,//Cuando no consigan la ruta
	Link,//Moverse entre rutas
} from 'react-router';
/*import { Route,Switch} from 'react-router-dom';
import { Link } from 'react-router-dom';*/

import Home from './Home.jsx';
import Post from './Post.jsx';
import Profile from './Profile.jsx';
import Error404 from './Error404.jsx';
import Header from '../../shared/components/Header.jsx';

function Pages(){
	return(
		<main role="application">
			<Header />
			<Match
				pattern="/"
				exactly
				component={Home}
			/>
		{/*Detalle de artículo*/}	
			<Match
				pattern ="/post/:id"
				exactly
				component={Post}
			/>
		{/* Perfil de usuario*/}	
			<Match
				pattern ="/user/:id"
				exactly
				component={Profile}
			/>
		{/* Error 404*/}
			<Miss component={Error404} />
		</main>
	)
}
export default Pages;