import React, {Component} from 'react';
import { Link } from 'react-router';
import { FormattedMessage } from 'react-intl';

/*import { Link } from 'react-router-dom';*/

import Post from '../../posts/container/Post.jsx';
import Loading from '../../shared/components/Loading.jsx';
import Header from '../../shared/components/Header.jsx';

import api from '../../api.js';

import styles from './Page.css';
class Home extends Component{
	//--
	//Creo un constructor ya que necesito crear metodos de maninuplación de la api
	constructor(props){
		super(props);

		this.state ={
			page:1,
			posts:[],
			loading:true,
		};
		this.handleScroll = this.handleScroll.bind(this);
	}
	//--
	async componentDidMount(){
		this.initialFetch();
		//Le decimos a nuestro componente que antes de que se monte escuche el evento
		window.addEventListener('scroll', this.handleScroll);
	}
	//Cuando el componente se desmonte dejamos de escuchar...
	componentWillUnmount() {
		window.removeEventListener('scroll',this.handleScroll);
	}
	//Para evitar hacer un doble render
	async initialFetch(){
		const posts = await api.posts.getList(this.state.page);
		//Para paginacion instancio el state cuando este montado el componente
		this.setState({
			posts,
			page: this.state.page + 1,
			loading:false
		})
	}

	handleScroll(){
		//Para evitar multiples request
		if(this.state.loading) return null;
		//
		const scrolled = window.scrollY;
		//--
		const viewportHeight = window.innerHeight;
		const fullHeight = document.body.clientHeight;
		//Si el usuario esta a 300px de finalizar la página se carfan 300 mas de post
		if(!(scrolled + viewportHeight + 300 >= fullHeight)){
			return null;
		}
		//--
		return this.setState({ loading: true }, async ()=>{
			//BLoque try ctach para capturar algun error
			try{
				const posts = await api.posts.getList(this.state.page);
				//Concateno el estado actual de los posts con los nuevos posts
				this.setState({
					posts: this.state.posts.concat(posts),
					page: this.state.page +1,
					loading: false,
				})

			}catch(error){
				console.error(error);
				this.setState({ loading: false});
			}
		});
		//--

	}

	render(){
		return(
			<section name="Home" className={styles.section}>
				<FormattedMessage id="title.home" />
				<section className={styles.list}>
					{this.state.loading &&(
						<Loading />
					)}
					{this.state.posts
						.map(post => <Post key={post.id} {...post}/>)
					}
				</section>

				<Link to="/profile">
					Go to profile
				</Link>
			</section>
		);
	}
}
export default Home;