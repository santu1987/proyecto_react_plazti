import React, {Component,PropTypes} from 'react';
import { FormattedMessage } from 'react-intl';

//import { Link } from 'react-router';
/*import { Link } from 'react-router-dom';*/
//Llamamos a Post y al api

import Post from '../../posts/container/Post.jsx';
import Loading from '../../shared/components/Loading.jsx';

import api from '../../api.js';

class Profile extends Component{
	//--
	//LLamamos al aconstructor....
	constructor(props){
		super(props);
		//Datos iniciales
		this.state = {
			user: {},
			posts: [],
			loading: true,
		};
	}
	//Al estar montado el componente...
	//metodo asincrono para capturar datos de usuario los posts de ese usuario...
	componentDidMount() {
		this.initialFetch();
	}
	async initialFetch(){
			const[
			user,
			posts,
		] = await Promise.all([
			//--
			api.users.getSingle(this.props.params.id),
			api.users.getPosts(this.props.params.id)
			//--
		]);
		this.setState({
			user,
			posts,
			loading:false,
		});
		console.log(this.state.user)
	}
	//--
	render(){
		if(this.state.loading){
			return <Loading />
		}
		return(
			<section name="Profile">
				<FormattedMessage 
						id="title.profile"
						values = {{
							name: this.state.user.name
						}}
				/>
				<h2>Profile of {this.state.user.name}</h2>
				<fieldset>
					<FormattedMessage id="profile.field.basic" tagName="legend" />
					<input type="email" value={this.state.user.email} disabled />
				</fieldset>
				{ this.state.user.address && (
					<fieldset>
						<FormattedMessage id="profile.field.address" tagName="legend" />
						<address>
							{ this.state.user.address.street } <br />
							{ this.state.user.address.suite } <br />
							{ this.state.user.address.city } <br />
							{ this.state.user.address.zipcode } <br />
						</address>
					</fieldset>
				)}
				<section>
					{this.state.posts
						.map(post => (
										<Post 
											key={post.id}
											user={this.state.user} 
											{...post}
										/>
									))
					}
				</section>
				//--- 
			</section>
		);
	}
}

Profile.propTypes = {
	params: PropTypes.shape({
		id:PropTypes.profile,
	}),
};

export default Profile;