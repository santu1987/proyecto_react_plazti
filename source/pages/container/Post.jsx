import React, {Component, PropTypes} from 'react';

/*import { Link } from 'react-router-dom';*/
import PostBody from '../../posts/container/Post.jsx';
import Loading from '../../shared/components/Loading.jsx';
import Comment from '../../comments/components/Comment.jsx';

import api from '../../api.js';

class Post extends Component{
	//constructor
	constructor(props){
		super(props);
	//--Definimos nuestro estado
		this.state = {
			loading:true,
			user:{},
			post:{},
			comments:[],
		};
	}	
	//--Carga de datos
	componentDidMount() {
		this.initialFetch();
	}
	//--
	async initialFetch(){
		const [
			post,
			comments,
		] = await Promise.all([
			api.posts.getSingle(this.props.params.id),
			api.posts.getComments(this.props.params.id)
		]);
		//No podemos mezclar las promesas del post y la del user por ello creamos otra constante
		const user = await api.users.getSingle(post.userId);
		//Setiamos
		this.setState({
			loading:false,
			user,
			post,
			comments,
		});
	}
	//--
	render(){
		if(this.state.loading){
			return <Loading />;
		}

		return(
			<section name="post">
				<PostBody
					{...this.state.post}
					user={this.state.user}
					comments={this.state.comments}
				/>
				<section>
					{this.state.comments
						.map(comment => (
							<Comment key={comment.id} {...comment} />
						))
					}
				</section>
			</section>
		);
	}
}

Post.propTypes = {
	params: PropTypes.shape({
		id:PropTypes.string,
	}),
}
export default Post;